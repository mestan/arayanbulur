# Generated by Django 2.2.1 on 2019-05-21 16:44

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import mptt.fields
import taggit.managers


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('taggit', '0003_taggeditem_add_unique_index'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True)),
                ('slug', models.SlugField()),
                ('lft', models.PositiveIntegerField(editable=False)),
                ('rght', models.PositiveIntegerField(editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(editable=False)),
                ('parent', mptt.fields.TreeForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='children', to='items.Category')),
            ],
            options={
                'verbose_name_plural': 'categories',
                'unique_together': {('parent', 'slug')},
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('parent_category', models.CharField(blank=True, max_length=72, null=True)),
                ('title', models.CharField(max_length=72)),
                ('slug', models.SlugField(blank=True, max_length=72, unique=True)),
                ('desc', models.TextField(max_length=5000, verbose_name='Description')),
                ('price', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('is_featured', models.BooleanField(default=False)),
                ('viewCount', models.IntegerField(default=0)),
                ('photo', models.CharField(max_length=500)),
                ('seo_keys', models.CharField(blank=True, max_length=160)),
                ('canonical_url', models.CharField(blank=True, max_length=160)),
                ('seo_description', models.CharField(blank=True, max_length=160)),
                ('feature_photo', models.CharField(blank=True, max_length=350)),
                ('alt', models.CharField(blank=True, max_length=160, null=True, verbose_name='Image alt attr')),
                ('is_published', models.BooleanField(default=False)),
                ('pub_date', models.DateTimeField(verbose_name='date published')),
                ('faved', models.BooleanField(blank=True, default=False, null=True)),
                ('category', mptt.fields.TreeForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='items.Category')),
                ('tags', taggit.managers.TaggableManager(blank=True, help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Taggit tags')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Item Owner')),
            ],
            options={
                'verbose_name': 'Item',
                'verbose_name_plural': 'Items',
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='BookmarkItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('favorited_at', models.DateTimeField(auto_now_add=True, null=True, verbose_name='Favorited at')),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='items.Item', verbose_name='Favorite Item')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Favorited by')),
            ],
            options={
                'verbose_name': 'Favorited Item',
                'verbose_name_plural': 'Favorited Items',
                'db_table': 'bookmark_item',
                'ordering': ['-favorited_at'],
            },
        ),
    ]
