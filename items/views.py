"""
This module handles requests

"""
# disable warning for get(format=None)
# pylint: disable=W0622
# disable warning for extra space in model fields
# pylint: disable=C0326
# disable warning for todos
# pylint: disable=W0511
# disable line too long
# pylint: disable=C0301
# from django.http import HttpResponse
import json
from django.shortcuts import render, get_object_or_404  # redirect
from django.views.generic import RedirectView
from django.http import HttpResponseRedirect
from django.utils import timezone
from django.views import generic
from django.views.generic.base import TemplateView
from django.contrib.auth.models import User
from rest_framework.views import APIView # AJAX like unlike
from rest_framework.response import Response
from rest_framework import authentication, permissions
# from statsy.mixins import WatchMixin

 
from django.contrib import auth
from django.http import HttpResponse
from django.views import View

# from django.contrib.auth.decorators import login_required
# from django.contrib.auth.forms import UserCreationForm
# from django.contrib.auth import authenticate, login, logout #get_user_model
# from django.views.generic import
from .models import Item, BookmarkItem, Category


# HTTP Methods
def home(request):
    ''' Normal html request handler '''
    items = Item.objects.filter().order_by('-id')[:] # is_published
    cats = Category.objects.filter().order_by('name')
    # sluglist =  Category.get_slug_list(cats) # nerede, nasil kullanılır ?
    user = request.user
    return render(request, "items/home.html", {"items": items, "user": user, 'cats': cats})

class Profile(generic.TemplateView):
    '''
    User's Public profile.
    A user can visit any other user profile and view wanted items by that user.
    User can visit its own profile and view items, delete, modify etc..
    User can view his own favorite (bookmarked) items
    BookmarkItem model has 'item' and 'favorited_at' fields.
    'item' is the foreign field and refers to the Item model

    '''
    template_name = 'items/profile.html'
    def get_context_data(self, **kwargs):
        context = super(Profile, self).get_context_data(**kwargs)
        userstring = self.kwargs['username'] # get argument from URL
        user_obj = User.objects.get(username=userstring) # get user as obj
        user = self.request.user
        context['items'] = Item.objects.filter(user=user_obj)  # get items for the user in the URL
        print(context['items'])
        if user.is_authenticated:
            context['favs'] = BookmarkItem.objects.filter(user=user_obj)
            context['user'] = self.request.user  # get user 
        return context


class ParentCategoryList(generic.TemplateView):
    '''
    When user clicks a parent category in the homepage, 
    lists all items belong to selected parent category.
    '''
    template_name = 'items/cats.html'
    def get_context_data(self, **kwargs):
        context = super(ParentCategoryList, self).get_context_data(**kwargs)
        slug = self.kwargs['slug'] # get slug for the parent category from URL
        items = Item.objects.filter(parent_category=slug) # use parent category slug to get list of items
        context['items'] = items
        all_child_categories = []
        for item in items:
            all_child_categories.append(item.category)
        context['categories'] = list(dict.fromkeys(all_child_categories)) # remove all duplicate categories and add to context
        return context



###################################
###################################
###################################
###################################
###################################
###################################
###   HENÜZ KULLANILMAYANLAR    ###
###################################
###################################
###################################
###################################
###################################
###################################



class IndexView(generic.ListView):
    '''
    !!! NOT USED !!! FBV is used instead for better control in displaying user favs
    'item_list' context variable is provided by default
    '''
    template_name = 'items/index.html'
    queryset = Item.objects.filter(
                pub_date__lte=timezone.now(),
                is_published=True,
                ).order_by('-pub_date')

    def get_context_data(self, **kwargs):
        ''' DOC '''
        # Call the base implementation first to get a context
        context = super(IndexView, self).get_context_data(**kwargs)
        user = self.request.user
        if user.is_authenticated:
            context['user_favs'] = BookmarkItem.objects.filter(user=user)
        return context

def index_view(request):
    '''
    if user is not logged in display all items as it is.
    When user is logged in check if the Items are faved by user
    And add to a list accordingly to use in template if check and
    show faved icon if it's faved or not faved icon accoringly.
    '''
    template_name = 'items/index.html'
    user = request.user
    allprod = Item.objects.filter(pub_date__lte=timezone.now(), is_published=True).order_by('-pub_date')
    # if user is not logged in
    if not user.is_authenticated:
        return render(request, template_name, {'product_list': allprod})
    # When user is logged in, continue..
    favorite_items = []
    favs_by = BookmarkItem.objects.filter(user=user)
    for item in allprod:
        for fav in favs_by:
            if fav.item.id == item.id:
                item.faved = True
        favorite_items.append(item)
    return render(request, template_name, {'product_list': favorite_items})



class BookmarkView(View):
    '''
    'product_id' BookmarkItem model içindeki 'product' field ile bağlantılı
    Ayrıca, index.html içindeki 'to_bookmarks()' ajax func'ın { 'product' : pk }
    ile bağlantılı. Yani product yerine obj isim değişikliği yapılırsa
    tüm product geçen kısımlar obj olarak değişmeli. örn: 'obj_id'
    '''
    # Passed from url.py
    model = None # This variable will set the bookmark model to be processed

    def post(self, request, pk):
        ''' doc '''
        # We need a user
        user = auth.get_user(request)
        # Trying to get a bookmark from the table, or create a new one
        bookmark, created = self.model.objects.get_or_create(user=user, product_id=pk)
        # If no new bookmark has been created,
        # Then we believe that the request was to delete the bookmark
        if not created:
            bookmark.delete()

        return HttpResponse(
            json.dumps({
                "result": created,
                "count": self.model.objects.filter(product_id=pk).count()
            }),
            content_type="application/json"
        )



class DetailView(generic.DetailView):
    '''
    'item' context variable is provided by default
    '''
    model = Item
    slug_url_kwarg = 'slug'
    template_name = 'items/detail.html'

    def get_queryset(self):
        """
        Return Item detail if pub_date is past and is_published true

        """
        return Item.objects.filter(
            pub_date__lte=timezone.now(),
            is_published=True,
            )

def redirect(request, slug, pid):
    '''
    if user is not logged in display all items as it is.
    When user is logged in check if the Items are faved by user
    And add to a list accordingly to use in template if check and
    show faved icon if it's faved or not faved icon accoringly.
    '''
    obj = get_object_or_404(Item, pk = pid)
    print(obj.link)
    return HttpResponseRedirect(obj.link)
