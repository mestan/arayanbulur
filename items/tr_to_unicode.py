'''
Türkçe karaketerleri unicode'a çevirir
DİKKAT: küçük 'ı' karakterlerini çeviremiyordu sonradan
replace_i() methodunu ekleyerek çözüldü.
'''
import re
import unicodedata

def replace_i(text):
    ''' Replace lowercase 'ı' char with 'i' '''
    text = text.replace('ı', r'i')
    return text

def strip_accents(text):
    """
    Strip accents from input String.

    :param text: The input string.
    :type text: String.

    :returns: The processed String.
    :rtype: String.
    """
    text = replace_i(text)
    try:
        text = unicode(text, 'utf-8')
    except (TypeError, NameError): # unicode is a default on python 3 
        pass
    text = unicodedata.normalize('NFD', text)
    text = text.encode('ascii', 'ignore')
    text = text.decode("utf-8")
    return str(text)

def convert_tr_chars_to_slug(text):
    """
    Convert input text to id.

    :param text: The input string.
    :type text: String.

    :returns: The processed String.
    :rtype: String.
    """
    text = strip_accents(text.lower())
    text = re.sub('[ ]+', '-', text)
    text = re.sub('[^0-9a-zA-Z_-]', '', text)
    return text
