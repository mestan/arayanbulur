'''
Items module's Model class

What is `related_name` used for in Django?
https://stackoverflow.com/questions/2642613/what-is-related-name-used-for-in-django/2642645#2642645

https://djangopy.org/package-of-week/categories-with-django-mptt/

'''
# disable warning for extra space in model fields
# pylint: disable=C0326
# disable warning for todos
# pylint: disable=W0511
# disable line too long
# pylint: disable=C0301

import datetime
from django.utils import timezone
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.db import models
from taggit.managers import TaggableManager
from mptt.models import MPTTModel, TreeForeignKey
# from django.conf import settings
from .tr_to_unicode import convert_tr_chars_to_slug


class Category(MPTTModel):
    ''' Item categories '''
    name = models.CharField(max_length=50, unique=True)
    parent = TreeForeignKey('self', null=True, blank=True, 
                            related_name='children', db_index=True, on_delete=models.SET_NULL)
    slug = models.SlugField()

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        unique_together = (('parent', 'slug',))
        verbose_name_plural = 'categories'

    def get_slug_list(self):
        ''' get slug list '''
        try:
            ancestors = self.get_ancestors(include_self=True)
        except: # pylint: disable=W0702
            ancestors = []
        else:
            ancestors = [ i.slug for i in ancestors]
            slugs = []
        for i in range(len(ancestors)):
            slugs.append('/'.join(ancestors[:i+1]))
        return slugs

    def save(self):
        ''' Override save method with custom implementations '''
        # Create and save slug field from the title
        self.slug = slugify(convert_tr_chars_to_slug(self.name))
        # Save every fields of the model instance
        super(Category, self).save()

    def __str__(self):
        return str(self.name).title()


class Item(models.Model):
    ''' Wanted Item Properties '''
    user            = models.ForeignKey(User, verbose_name="Item Owner", on_delete=models.CASCADE)
    category        = TreeForeignKey('Category',null=True,blank=True, on_delete=models.SET_NULL)
    parent_category = models.CharField(max_length=72, blank=True, null=True)
    title           = models.CharField(max_length=72)
    slug            = models.SlugField(max_length=72, unique=True, blank=True)
    desc            = models.TextField(max_length=5000, verbose_name="Description")
    price           = models.IntegerField(default=0)
    created_at      = models.DateTimeField(auto_now_add=True, verbose_name="Created at")
    is_featured     = models.BooleanField(default=False)
    viewCount       = models.IntegerField(default=0)
    photo           = models.CharField(max_length=500)
    seo_keys        = models.CharField(max_length=160, blank=True )
    canonical_url   = models.CharField(max_length=160, blank=True )
    seo_description = models.CharField(max_length=160, blank=True )
    tags            = TaggableManager(blank=True, verbose_name="Taggit tags")
    feature_photo   = models.CharField(max_length=350, blank=True) # only if is_featured = True
    alt             = models.CharField(max_length=160, blank=True, null=True, verbose_name="Image alt attr") # <img> alt attr for better seo
    is_published    = models.BooleanField(default=False) # for auto publishing needs to be checked.
    pub_date        = models.DateTimeField('date published') # Provides auto publish feature.
    faved           = models.BooleanField(default=False, blank=True, null=True) # for saving favs during view render

    class Meta:
        verbose_name = 'Item'
        verbose_name_plural = 'Items'
        ordering = ['-created_at']

    def favs(self):
        ''' DOC '''
        return self.bookmarkitem_set.all().count()

    def get_absolute_url(self):
        ''' Absolute URL '''
        return reverse("items:detail", args=[self.slug])

    def get_like_url(self):
        ''' Like with page refresh '''
        return reverse("items:like-toggle", args=[self.slug])

    def get_redirect_url(self):
        ''' Like with page refresh '''
        return reverse("items:redirect", args=[self.slug, self.id])

    def get_api_like_url(self):
        ''' Like with api call '''
        return reverse("items:like-api-toggle", args=[self.slug])

    def was_published_recently(self):
        '''  Return Boolean for Items in given time '''
        now = timezone.now()
        return now - datetime.timedelta(days=7) <= self.pub_date <= now

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        ''' Override save method with custom implementations '''
        # Create and save slug field from the title
        self.slug = slugify(self.title)

        # Save the parent categoy of the item
        try:
            self.parent_category = self.category.parent.slug
        except:
            pass
        # If empty, same with title.
        if not self.alt:
            self.alt = self.title
        # if empty same with body's first 160 char.
        if not self.seo_description:
            self.seo_description = self.desc[:160]
        # base url + absolute url
        self.canonical_url = 'https://luxxspot.com' + self.get_absolute_url()
        # Save every fields of the model instance
        super(Item, self).save()

    def __str__(self):
        # display title case in the admin panel
        return str(self.title).title()


class BookmarkBase(models.Model):
    ''' DOC '''
    class Meta:
        abstract = True

    user = models.ForeignKey(User, verbose_name="Favorited by", on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username

class BookmarkItem(BookmarkBase):
    ''' DOC '''
    class Meta:
        verbose_name = 'Favorited Item'
        verbose_name_plural = 'Favorited Items'
        db_table = "bookmark_item"
        ordering = ['-favorited_at']

    item = models.ForeignKey(Item, verbose_name="Favorite Item", on_delete=models.CASCADE)
    favorited_at = models.DateTimeField(auto_now_add=True, verbose_name="Favorited at", blank=True, null=True)
