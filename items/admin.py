'''

Items Admin

if operational error - no such table - Django, mptt
run this command:
python manage.py migrate --run-syncdb

'''
from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from .models import Item, Category, BookmarkItem

class CategoryAdmin(MPTTModelAdmin):
    '''
    Normally timestamps don't show up on admin page
    Use this custom class to register those fields
    note: "favs" in list_diplay is a model function
    '''
    list_display = ('name', 'parent')
    prepopulated_fields = {'slug': ('name',)}

class ItemAdmin(admin.ModelAdmin):
    '''
    Normally timestamps don't show up on admin page
    Use this custom class to register those fields
    note: "favs" in list_diplay is a model function
    '''
    def favlar(self, obj):
        '''As an example - no functinality unles put name in the list_display '''
        return obj.bookmarkitem_set.count()

    readonly_fields = ('created_at',)
    list_display = ('title', 'price', 'favs', 'viewCount', 'pub_date', 'is_featured', 'is_published')

    # automates entering of a slug in the admin panel
    prepopulated_fields = {'slug': ('title',)}

class FavoriteAdmin(admin.ModelAdmin):
    '''
    Normally timestamps don't show up on admin page
    Use this custom class to register those fields
    note: "favs" in list_diplay is a model function
    '''
    def favs(self, obj):
        return obj.item_set.count()
    # readonly_fields = ('favorited_at',)
    list_display = ('item', 'user', 'favs', 'favorited_at')


admin.site.register(Item, ItemAdmin)
admin.site.register(BookmarkItem, FavoriteAdmin)

admin.site.register(Category, CategoryAdmin)
