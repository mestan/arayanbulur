''' Doc '''
from django.urls import path, include
from django.contrib.auth.decorators import login_required
from rest_framework import routers
from . import views
from .api.views import ItemView
from .models import BookmarkItem

app_name = 'items'

ROUTER = routers.DefaultRouter()
# get product list at the root url
ROUTER.register('', ItemView)

urlpatterns = [
    path('api/v1/', include(ROUTER.urls)),
    path('api-auth/', include("rest_framework.urls")),
    path('users/<username>/', views.Profile.as_view(), name='profile'),
    path('detail/<slug:slug>/', views.DetailView.as_view(), name='detail'),
    path('cat/<slug:slug>/', views.ParentCategoryList.as_view(), name='parent_category_list'),
    path('to/<slug:slug>/<int:pid>', views.redirect, name='redirect'),
    path('api/product/<int:pk>/bookmark/', login_required(views.BookmarkView.as_view(model=BookmarkItem)), name='article_bookmark'),
    path('', views.home, name='home'),  # at the bottom

]
