"""
This module handles requests

"""
# disable warning for get(format=None)
# pylint: disable=W0622
from rest_framework import viewsets
from rest_framework.permissions import BasePermission, IsAdminUser, SAFE_METHODS # IsAuthenticated
from rest_framework.response import Response
from items.api.serializer import ItemSerializer
from items.models import Item

# API Methods
class ReadOnly(BasePermission):
    '''
    Prevent non-admin users to send post requests.
    Use below: permission_classes = (IsAdminUser|ReadOnly,)
    And -> def get(self, request, format=None) method
    '''
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS

class ItemView(viewsets.ModelViewSet): # pylint: disable=too-many-ancestors
    '''
    All Items View
    for slug URL use lookup_field below
    and add lookup_field & extra_kwargs: in the serializer.py -> ProductsSerializer
    '''
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    lookup_field = 'slug'
    permission_classes = (IsAdminUser|ReadOnly,)

    def get(self, request, format=None):
        ''' Only allow safe methods for non-admin users (GET) '''
        content = {
            'status': 'this request not allowed'
        }
        return Response(content)
