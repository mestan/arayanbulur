""" This module serializes Product model """
 # pylint: disable=C0326
from rest_framework import serializers
from items.models import Item

class ItemSerializer(serializers.ModelSerializer):
    """ Returns json response with all Item objects """

    class Meta:
        '''
        <url> field comes from rest_framework and creates link for item detail

        lookup_field & extra_kwargs: make slug URL
        Also need to add <lookup_field = 'slug'> in the views.py -> class ProductsView

        '''
        model   = Item
        fields  = ('url', 'id', 'slug', 'title', 'price', 'created_at', 'updated_at')
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }
