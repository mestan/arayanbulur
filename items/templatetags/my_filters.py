'''
My custom template filter
'''
from django import template
import re
import unicodedata

register = template.Library()

@register.filter(name='trupper')
def trupper(value):
    '''
    converts lowercase 'i' turkish character to 'İ' character
    '''
    value = value.replace('i', r'İ')
    return value.upper()


